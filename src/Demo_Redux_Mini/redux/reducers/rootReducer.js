import { combineReducers } from "redux";
import { numberReducer } from "./numberReducer";

export let rootReducer_demoReduxMini = combineReducers({
  numberReducer,
});
