import { type } from "@testing-library/user-event/dist/type";
import React, { Component } from "react";
import { connect } from "react-redux";
import {
  giamSoLuongAction,
  tangsoLuongAction,
} from "./redux/action/numberAction";

class Demo_Redux_Mini extends Component {
  render() {
    return (
      <div>
        <p>{this.props.soLuong}</p>
        <button
          onClick={() => {
            this.props.tangSoLuong(1);
          }}
          className="btn btn-success"
        >
          Tăng
        </button>
        <br />
        <br />
        <button
          onClick={() => {
            this.props.giamSoLuong(1);
          }}
          className="btn btn-dark"
        >
          Giảm
        </button>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    soLuong: state.numberReducer.number,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    tangSoLuong: (soLuong) => {
      dispatch(tangsoLuongAction(soLuong));
    },
    giamSoLuong: (soLuong) => {
      dispatch(giamSoLuongAction(soLuong));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Demo_Redux_Mini);
