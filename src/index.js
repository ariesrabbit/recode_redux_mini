import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { Provider } from "react-redux"; //thêm dòng này
import { createStore } from "redux"; // thêm dòng này
import { rootReducer_demoReduxMini } from "./Demo_Redux_Mini/redux/reducers/rootReducer"; //thêm dòng này
const root = ReactDOM.createRoot(document.getElementById("root"));
const store = createStore(
  rootReducer_demoReduxMini,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);
root.render(
  <Provider store={store}>
    <App />
  </Provider>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
