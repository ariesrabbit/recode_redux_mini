import { GIAM_SO_LUONG, TANG_SO_LUONG } from "./../constant/numberConstant";
export const tangsoLuongAction = (soLuong) => {
  return {
    type: TANG_SO_LUONG,
    payload: soLuong,
  };
};
export const giamSoLuongAction = (soLuong) => {
  return {
    type: GIAM_SO_LUONG,
    payload: soLuong,
  };
};
