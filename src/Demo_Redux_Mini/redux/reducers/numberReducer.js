import { GIAM_SO_LUONG, TANG_SO_LUONG } from "../constant/numberConstant";
let initialState = {
  number: 10,
};

export const numberReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case TANG_SO_LUONG:
      state.number += payload;
      return { ...state };
    case GIAM_SO_LUONG:
      state.number -= payload;
      return { ...state };
    default:
      return state;
  }
};
